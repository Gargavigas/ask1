package ask1;
/**
 *
 * @author Thanasis
 */
public class HistoryLog {
    @SuppressWarnings("FieldMayBeFinal")
    private Contact[] calledContacts = new Contact[1000];
    
    public int getCalledContactsNumber() {
        return 0;
    }
    
    public boolean isFull() {
        for(Contact calledContact : calledContacts) {
            if (calledContact == null) {
                return false;
            }
        }
        return true;
    }
    public boolean call(Contact contact) {
        for (int i = 0; i < calledContacts.length; i++) {
            if (calledContacts[i] == null) {
                calledContacts[i] = contact;
                return true;
            }
        }
        return false;
    }
}
