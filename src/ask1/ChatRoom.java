package ask1;
import java.util.ArrayList;

/**
 *
 * @author Thanasis
 */
public class ChatRoom {
    public int myPhoneNumber;
    public ArrayList <Contact> contacts;
    
    public ChatRoom(int myPhoneNumber) {
        this.myPhoneNumber = myPhoneNumber;
        this.contacts = new ArrayList <Contact> ();
    }
    
    public boolean addChatContact(Contact contact) {
        if (findPosition(contact) >=0) {
            System.out.println("η επαφή υπάρχει ήδη στο chat Room");
            return false;
        } else {
            contacts.add(contact);
        }
        return true;
    }
    
    public ArrayList <Contact> getContacts() {
        return contacts;
    }
    
    private int findPosition(Contact contact) {
        return this.contacts.indexOf(contact);
    }
    private int findPosition(String name) {
        for (int i=0; i<contacts.size(); i++) {
            Contact contact = this.contacts.get(i);
            if (contact.getName().equals(name)){
                return i;
            }
        }
        return -1;
    }
    
}
