package ask1;

/**
 *
 * @author Thanasis
 */
public class Main {
    public static void main(String[] args) {
        Contact cont1 = new Contact();
        cont1.name = "Athanasios Rokopoulos";
        cont1.phoneNumber = "6951407803";
        
        HistoryLog g = new HistoryLog();
        boolean check = g.call(cont1);
        System.out.println(check);
    }
}
